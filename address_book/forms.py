from django.forms import ModelForm

from django.forms.formsets import BaseFormSet, formset_factory
from django.forms.models import inlineformset_factory, BaseInlineFormSet
from django.forms import ValidationError

from .models import Contact, Email, PhoneNumber, Address, Group


class RequiredFormSet(BaseFormSet):
    """
        RequiredFormSet to set required fields of form
        and to not allow empty values
    """

    def __init__(self, *args, **kwargs):
        super(RequiredFormSet, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = False


class ContactGroupForm(ModelForm):

    """
        Contact Group Form - to display to user to add new group
    """

    class Meta:
        model = Group
        fields = ('name',)


class ContactForm(ModelForm):

    """
        Contact Form - to display to user to add new contact
    """

    class Meta:
        model = Contact
        fields = ['first_name', 'last_name', 'middle_name', 'title', 'groups']


class EmailForm(ModelForm):

    """
        Email Form - to display to user to add new email
    """

    class Meta:
        model = Email
        fields = ('email', 'type')


class PhoneForm(ModelForm):

    """
        Phone Form - to display to user to add new phone
    """

    class Meta:
        model = PhoneNumber
        fields = ('phone', 'type')


class AddressForm(ModelForm):

    """
        Address Form - to display to user to add new address
    """

    class Meta:
        model = Address
        fields = ('street', 'city', 'state', 'zip', 'type')


class MandatoryInlineFormSet(BaseInlineFormSet):

    def is_valid(self):
        return super(MandatoryInlineFormSet, self).is_valid() and \
            not any([bool(e) for e in self.errors])

    def clean(self):
        count = 0
        for form in self.forms:
            try:
                if form.cleaned_data and not form.cleaned_data.get('DELETE', False):
                    count += 1
            except AttributeError:
                pass
        if count < 1:
            raise ValidationError('You must have at least one of these.')

PhoneFormSet = formset_factory(PhoneForm, max_num=3, formset=RequiredFormSet)
AddressFormSet = formset_factory(AddressForm, max_num=3, formset=RequiredFormSet)
EmailFormSet = formset_factory(EmailForm, max_num=3, formset=RequiredFormSet)


EmailEditFormSet = inlineformset_factory(
    Contact,
    Email,
    extra=1,
    formset=MandatoryInlineFormSet,
    fields=(
        'email',
        'type'))
PhoneEditFormSet = inlineformset_factory(
    Contact,
    PhoneNumber,
    extra=1,
    can_delete=True,
    fields=(
        'phone',
        'type'))
AddressEditFormSet = inlineformset_factory(
    Contact,
    Address,
    extra=1,
    formset=MandatoryInlineFormSet, fields=('street', 'city', 'state', 'zip', 'type'))
