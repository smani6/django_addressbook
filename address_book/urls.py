from django.conf.urls import patterns, url
from .views import ContactCreateView, ContactsView, GroupsView, GroupCreateView, GroupUpdateView, SearchView, Dashboard, GroupDetailView, ContactDetailView, ContactsUpdateView

urlpatterns = [

    url(r'^contacts/$', ContactsView.as_view(), name='contacts'),
    url(r'^contacts/new/$',
        ContactCreateView.as_view(), name="contact_new"),
    url(r'contacts/(?P<pk>\d+)/$',
        ContactDetailView.as_view(), name="contact_detail"),
    url(r'^contacts/(?P<pk>\d+)/edit/$',
        ContactsUpdateView.as_view(), name="contact_edit"),

    url(r'groups/$', GroupsView.as_view(), name='groups'),
    url(r'groups/new/$', GroupCreateView.as_view(), name='group_new'),
    url(r'groups/(?P<pk>\d+)/$',
        GroupDetailView.as_view(), name="group_detail"),
    url(r'^groups/(?P<pk>\d+)/edit/$',
        GroupUpdateView.as_view(), name="group_edit"),

    url(r'^$', Dashboard.as_view(), name='dashboard'),

    url(r'^search/$', SearchView.as_view(), name="search")
]
