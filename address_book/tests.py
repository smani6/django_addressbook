from django.test import TestCase, RequestFactory
from address_book.views import ContactCreateView, GroupCreateView, GroupDetailView
from django import http, shortcuts
import mock
from mock import patch
from mock import Mock
from django.shortcuts import render
from django.core.urlresolvers import reverse
# Create your tests here.


class AddressBookTestCases(TestCase):

    def setUp(self):
        self.factory = RequestFactory()

    @patch('address_book.views.render')
    def test_get_add_new_contact(self, render):
        request = self.factory.get(reverse('contact_new'), {})

        def mock_render(request, template, context):
            assert template == 'add_contact.html'
            return shortcuts.render(request, template, context)

        render.side_effect = mock_render
        response = ContactCreateView.as_view()(request)
        assert render.called
        self.assertEqual(response.status_code, 200)

    @patch('address_book.views.render')
    def test_post_add_new_contact(self, render):

        data = {
            "first_name": "Mark",
            "last_name": "Zukerberg",
            "middle_name": "",
            "title_name": "",
            "groups": "1",
            "email-TOTAL_FORMS": "1",
            "email-INITIAL_FORMS": "0",
            "email-MIN_NUM_FORMS": "0",
            "email-MAX_NUM_FORMS": "3",
            "email-0-email": "sadfs@gmail.com",
            "email-0-type": "Personal",
            "phone-TOTAL_FORMS": "1",
            "phone-INITIAL_FORMS": "0",
            "phone-MIN_NUM_FORMS": "0",
            "phone-MAX_NUM_FORMS": "3",
            "phone-0-phone": "131424",
            "phone-0-type": "Mobile",
            "address-TOTAL_FORMS": "1",
            "address-INITIAL_FORMS": "0",
            "address-MIN_NUM_FORMS": "0",
            "address-MAX_NUM_FORMS": "3",
            "address-0-street": "Street",
            "address-0-city": "City",
            "address-0-state": "State",
            "address-0-zip": "Zip",
            "address-0-type": "Home"
        }

        request_group_create = self.factory.post(reverse('contact_new'), {'name': 'Friends'})
        response_group_create = GroupCreateView.as_view()(request_group_create)

        request = self.factory.post('/contact/new', data)

        def mock_render(request, template, context):
            return shortcuts.render(request, template, context)

        render.side_effect = mock_render

        def mock_HttpResponseRedirect(val):
            return True

        ContactCreateView.HttpResponseRedirect = Mock(side_effect=mock_HttpResponseRedirect)

        response = ContactCreateView.as_view()(request)

        self.assertEqual(response.status_code, 200)

    @patch('address_book.views.render')
    def test_get_add_new_group(self, render):
        request = self.factory.get(reverse('group_new'), {})

        def mock_render(request, template, context):
            assert template == 'add_group.html'
            return shortcuts.render(request, template, context)

        render.side_effect = mock_render
        response = GroupCreateView.as_view()(request)
        assert render.called
        self.assertEqual(response.status_code, 200)

    def test_post_add_new_group(self):
        request = self.factory.post(reverse('group_new'), {'name': 'friends'})
        response = GroupCreateView.as_view()(request)
        self.assertEqual(response.status_code, 302)

    def test_group_detail_view(self):

        data = {
            "first_name": "Mark",
            "last_name": "Zukerberg",
            "middle_name": "",
            "title_name": "",
            "groups": "1",
            "email-TOTAL_FORMS": "1",
            "email-INITIAL_FORMS": "0",
            "email-MIN_NUM_FORMS": "0",
            "email-MAX_NUM_FORMS": "3",
            "email-0-email": "sadfs@gmail.com",
            "email-0-type": "Personal",
            "phone-TOTAL_FORMS": "1",
            "phone-INITIAL_FORMS": "0",
            "phone-MIN_NUM_FORMS": "0",
            "phone-MAX_NUM_FORMS": "3",
            "phone-0-phone": "131424",
            "phone-0-type": "Mobile",
            "address-TOTAL_FORMS": "1",
            "address-INITIAL_FORMS": "0",
            "address-MIN_NUM_FORMS": "0",
            "address-MAX_NUM_FORMS": "3",
            "address-0-street": "Street",
            "address-0-city": "City",
            "address-0-state": "State",
            "address-0-zip": "Zip",
            "address-0-type": "Home"
        }

        request_group_create = self.factory.post(reverse('contact_new'), {'name': 'Friends'})
        response_group_create = GroupCreateView.as_view()(request_group_create)

        request_contact_create = self.factory.post('/contact/new', data)

        def mock_render(request, template, context):
            return shortcuts.render(request, template, context)

        render.side_effect = mock_render

        def mock_HttpResponseRedirect(val):
            return True

        ContactCreateView.HttpResponseRedirect = Mock(side_effect=mock_HttpResponseRedirect)

        response_contact_create = ContactCreateView.as_view()(request_contact_create)

        request = self.factory.get('/groups/1', {})
        response = GroupDetailView.as_view()(request, 1)
        self.assertEqual(response.status_code, 200)
