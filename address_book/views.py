import json
from django.shortcuts import render
from django.views.generic import View
from django.template.context import RequestContext
from .forms import ContactForm, EmailFormSet, PhoneFormSet, AddressFormSet, ContactGroupForm
from .models import Group, Contact, GroupMember, PhoneNumber, Email
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.views.generic import (
    ListView, DetailView,
    UpdateView, CreateView, DeleteView)
from django.views.generic.edit import ModelFormMixin
from django.core.urlresolvers import reverse_lazy


class ContactCreateView(View):

    """
        View to create new contacts
    """

    def get(self, request):
        """
            Get method to return the new contact form
            to user
        """
        contact_form = ContactForm()
        email_formset = EmailFormSet(prefix="email")
        phone_formset = PhoneFormSet(prefix="phone")
        address_formset = AddressFormSet(prefix="address")

        return render(request, 'add_contact.html',
                      RequestContext(request, {
                          'phone_formset': phone_formset,
                          'contact_form': contact_form,
                          'email_formset': email_formset,
                          'address_formset': address_formset
                      }))

    def post(self, request):
        """
            POST method to create new contact
            validates the input form data - if form is not valid
            return form with errors
            else: create new contact details and
            subsequent contact email, phone, address details
        """

        try:
            contact_form = ContactForm(request.POST)
            email_formset = EmailFormSet(request.POST, prefix="email")
            phone_formset = PhoneFormSet(request.POST, prefix="phone")
            address_formset = AddressFormSet(request.POST, prefix="address")

            if not contact_form.is_valid():
                return form_error_context(
                    request,
                    contact_form,
                    email_formset,
                    phone_formset,
                    contact_form,
                    address_formset)

            if not email_formset.is_valid():
                return form_error_context(
                    request,
                    email_formset,
                    email_formset,
                    phone_formset,
                    contact_form,
                    address_formset)

            if not phone_formset.is_valid():
                return form_error_context(
                    request,
                    phone_formset,
                    email_formset,
                    phone_formset,
                    contact_form,
                    address_formset)

            if not address_formset.is_valid():
                return form_error_context(
                    request,
                    address_formset,
                    email_formset,
                    phone_formset,
                    contact_form,
                    address_formset)

            if (contact_form.is_valid() and email_formset.is_valid()
                    and phone_formset.is_valid() and address_formset.is_valid()):

                contact = contact_form.save(commit=False)
                contact.save()

                for group_id in request.POST.getlist('groups'):
                    group_obj = Group.objects.get(id=group_id)
                    membership = GroupMember.objects.create(contact=contact, group=group_obj)

                for form in email_formset.forms:
                    email = form.save(commit=False)
                    email.contact = contact
                    email.save()
                for form in phone_formset.forms:
                    phone = form.save(commit=False)
                    phone.contact = contact
                    phone.save()
                for form in address_formset.forms:
                    address = form.save(commit=False)
                    address.contact = contact
                    address.save()
                return HttpResponseRedirect(reverse('dashboard'))

        except Exception as e:
            print e
            return HttpResponseRedirect(reverse('dashboard'))


class Dashboard(View):

    """
        View to return the dashboard - home page of website
    """

    def get(self, request):
        """
            Get method to return contact details
            to be displayed in home page
        """

        groups = Group.objects.all()
        contacts = Contact.objects.all()

        for contact in contacts:
            group_details = GroupMember.objects.filter(contact_id=contact.id)
            contact.phone_info = PhoneNumber.objects.filter(contact_id=contact.id)
            contact.group_names = group_details

        return render(
            request, 'dashboard1.html', RequestContext(
                request, {
                    'group_contacts': [], 'contacts': contacts, 'groups': groups}))


class GroupsView(View):
    """
        View to return all groups of addressbook
    """

    def get(self, request):
        """
            Get method to all groups of addressbook
        """
        groups = Group.objects.all()
        return render(request, 'groups_view.html', {'groups': groups})


class ContactsView(View):
    """
        View to return all contacts of addressbook
    """

    def get(self, request):
        """
            View to return all contacts and details of addressbook
        """
        contacts = Contact.objects.all()
        for contact in contacts:
            group_details = GroupMember.objects.filter(contact_id=contact.id)
            phone_info = PhoneNumber.objects.filter(contact_id=contact.id)
            email_info = Email.objects.filter(contact_id=contact.id)
            contact.phone_info = phone_info
            contact.email_info = email_info
            contact.group_info = group_details
        return render(request, 'contacts_view.html', {'contacts': contacts})


class GroupCreateView(View):

    """
        View to create new group
    """

    def get(self, request):
        """
            View to return add group form to user
        """

        form = ContactGroupForm()
        return render(request, 'add_group.html', {'form': form})

    def post(self, request):
        """
            View to create new group - validates
            the form data and if valid will create new
            group else return form with errors
        """
        try:
            form = ContactGroupForm(request.POST)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(reverse('dashboard'))
            else:
                return render(request, 'add_group.html', {'form': form})
        except Exception as e:
            print e


class GroupDetailView(View):
    """
        View to return group with group member details
    """

    def get(self, request, pk):
        contacts = Contact.objects.filter(groups=pk)
        return render(request, 'group_detail.html', {'contacts': contacts})


class GroupUpdateView(UpdateView):
    """
        View to update group details
    """

    model = Group
    fields = ['name']
    template_name = 'edit_group.html'


class ContactsUpdateView(UpdateView):
    """
        View to update contacts details
    """

    model = Contact
    fields = ['title', 'first_name', 'middle_name', 'last_name', 'groups']
    template_name = 'edit_contact.html'
    success_url = reverse_lazy('contacts')

    def form_valid(self, form):

        self.object = form.save(commit=False)
        GroupMember.objects.filter(contact=self.object).delete()
        self.object.save()

        for groups in form.cleaned_data['groups']:
            obj = GroupMember()
            obj.contact = self.object
            obj.group = groups
            obj.save()

        return super(ModelFormMixin, self).form_valid(form)


class ContactDetailView(View):

    """
        View to return contact details
    """

    def get(self, request, pk):

        contact = Contact.objects.filter(id=pk)
        phone_info = PhoneNumber.objects.filter(contact=contact)
        group_details = GroupMember.objects.filter(contact=contact)

        return render(
            request, 'contact_detail.html', {
                'contact_details': contact, 'phone_info': phone_info, 'group_info': group_details})


class SearchView(View):

    """
        View to search addressbook with email, first_name, last_name, group_name
    """

    def get(self, request):
        return render(request, 'search_page.html')

    def post(self, request):

        try:

            data = json.loads(request.body)
            filter_type = data['filter_type']
            search_text = data['search_text']

            search_mapping = {
                "email_contains": search_by_email_substring,
                "email_exact_match": search_by_email_exact,
                "first_name": search_by_first_name,
                "last_name": search_by_last_name,
                "group_name": search_by_group
            }

            search_method = search_mapping[filter_type]

            search_matching_results = search_method(search_text)

            return HttpResponse(json.dumps(search_matching_results))
        except Exception as e:
            print e


def search_by_email_substring(text):
    """
        Method to search addressbook with email substring
    """

    email_matching_results = Email.objects.filter(email__icontains=text)
    result = []
    for email in email_matching_results:
        contact = Contact.objects.get(pk=email.contact.id)
        contact_info = derive_contact_info(contact)

        group_info = GroupMember.objects.filter(contact=contact)
        group_list = derive_group_info(group_info)

        phone_info = PhoneNumber.objects.filter(contact=contact)
        phone_list = derive_phone_info(phone_info)

        email_info = Email.objects.filter(contact=contact)
        email_list = derive_email_info(email_info)

        result.append({'contact_info': contact_info,
                       'phone_info': phone_list,
                       'group_info': group_list,
                       'email_info': email_list})

    print result
    return result


def search_by_email_exact(text):
    """
        Method to search addressbook with exact email match
    """

    email_matching_results = Email.objects.filter(email__exact=text)
    result = []
    for email in email_matching_results:
        contact = Contact.objects.get(pk=email.contact.id)
        contact_info = derive_contact_info(contact)

        group_info = GroupMember.objects.filter(contact=contact)
        group_list = derive_group_info(group_info)

        phone_info = PhoneNumber.objects.filter(contact=contact)
        phone_list = derive_phone_info(phone_info)

        email_info = Email.objects.filter(contact=contact)
        email_list = derive_email_info(email_info)

        result.append({'contact_info': contact_info,
                       'phone_info': phone_list,
                       'group_info': group_list,
                       'email_info': email_list})

    return result


def search_by_first_name(text):
    """
        Method to search addressbook with substring or exact match of first name
    """

    first_name_matching_results = Contact.objects.filter(first_name__icontains=text)
    result = []
    for cont in first_name_matching_results:
        contact = Contact.objects.get(pk=cont.id)
        contact_info = derive_contact_info(contact)

        group_info = GroupMember.objects.filter(contact=contact)
        group_list = derive_group_info(group_info)

        phone_info = PhoneNumber.objects.filter(contact=contact)
        phone_list = derive_phone_info(phone_info)

        email_info = Email.objects.filter(contact=contact)
        email_list = derive_email_info(email_info)

        result.append({'contact_info': contact_info,
                       'phone_info': phone_list,
                       'group_info': group_list,
                       'email_info': email_list})

    return result


def search_by_last_name(text):
    """
        Method to search addressbook with substring or exact match of last name
    """

    last_name_matching_results = Contact.objects.filter(last_name__icontains=text)
    result = []
    for cont in last_name_matching_results:
        contact = Contact.objects.get(pk=cont.id)
        contact_info = derive_contact_info(contact)

        group_info = GroupMember.objects.filter(contact=contact)
        group_list = derive_group_info(group_info)

        phone_info = PhoneNumber.objects.filter(contact=contact)
        phone_list = derive_phone_info(phone_info)

        email_info = Email.objects.filter(contact=contact)
        email_list = derive_email_info(email_info)

        result.append({'contact_info': contact_info,
                       'phone_info': phone_list,
                       'group_info': group_list,
                       'email_info': email_list})

    return result


def search_by_group(text):
    """
        Method to search addressbook with substring or exact match of group name
    """

    group_name_matching_results = Group.objects.filter(name__icontains=text)
    result = []

    for group in group_name_matching_results:
        contact_details = GroupMember.objects.filter(group=group)

        for cont in contact_details:

            contact = Contact.objects.get(pk=cont.contact_id)
            contact_info = derive_contact_info(contact)

            group_info = GroupMember.objects.filter(contact=contact)
            group_list = derive_group_info(group_info)

            phone_info = PhoneNumber.objects.filter(contact=contact)
            phone_list = derive_phone_info(phone_info)

            email_info = Email.objects.filter(contact=contact)
            email_list = derive_email_info(email_info)

            result.append({'contact_info': contact_info,
                           'phone_info': phone_list,
                           'group_info': group_list,
                           'email_info': email_list})

    return result


def derive_contact_info(contact):
    """
        Method to derive contact details from contact object and return dict
    """

    contact_dict = {}
    contact_dict['title'] = contact.title
    contact_dict['first_name'] = contact.first_name
    contact_dict['last_name'] = contact.last_name
    contact_dict['middle_name'] = contact.middle_name

    return contact_dict


def derive_group_info(group_info):
    """
        Method to derive group details from group object and return group list
    """
    group_list = []
    for group in group_info:
        group_list.append(group.group.name)

    return group_list


def derive_phone_info(phone_info):
    """
        Method to derive phone details from phone object and return phone list
    """
    phone_list = []
    for phone in phone_info:
        phone_list.append({'type': phone.type, 'phone': phone.phone})

    return phone_list


def derive_email_info(email_info):
    """
        Method to derive email details from email object and return email list
    """
    email_list = []
    for email in email_info:
        email_list.append({'type': email.type, 'email': email.email})

    return email_list


def form_error_context(
        request,
        form_name,
        email_formset,
        phone_formset,
        contact_form,
        address_formset):
    """
        Generic Method to return form - it can be contact or phone or email or address
        form with errors to user
    """

    return render(request, 'add_contact.html', {'form': form_name,
                                                'phone_formset': phone_formset,
                                                'contact_form': contact_form,
                                                'email_formset': email_formset,
                                                'address_formset': address_formset
                                                })
