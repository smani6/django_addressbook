# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('street', models.CharField(max_length=b'50')),
                ('city', models.CharField(max_length=b'50')),
                ('state', models.CharField(max_length=b'50')),
                ('zip', models.CharField(max_length=b'10')),
                ('type', models.CharField(max_length=b'10', choices=[(b'Home', b'Home'), (b'Work', b'Work')])),
            ],
        ),
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=b'100', verbose_name=b'First Name')),
                ('last_name', models.CharField(max_length=b'100', verbose_name=b'Last Name')),
                ('middle_name', models.CharField(max_length=b'100', null=True, verbose_name=b'Middle Name', blank=True)),
                ('title_name', models.CharField(max_length=b'100', null=True, verbose_name=b'Title', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Email',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.EmailField(max_length=254)),
                ('type', models.CharField(max_length=b'20', choices=[(b'Personal', b'Personal'), (b'Official1', b'Official1'), (b'Official2', b'Official2')])),
                ('contact', models.ForeignKey(related_name='email_contact', to='address_book.Contact')),
            ],
        ),
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=b'100', verbose_name=b'Group Name')),
            ],
        ),
        migrations.CreateModel(
            name='GroupMember',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('contact', models.ForeignKey(related_name='membership', to='address_book.Contact')),
                ('group', models.ForeignKey(related_name='membership', to='address_book.Group')),
            ],
        ),
        migrations.CreateModel(
            name='PhoneNumber',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('phone', models.CharField(max_length=b'20')),
                ('type', models.CharField(max_length=b'20', choices=[(b'Mobile', b'Mobile'), (b'Work', b'Work'), (b'Fax', b'Fax'), (b'Skype', b'Skype')])),
                ('contact', models.ForeignKey(related_name='phone_contact', to='address_book.Contact')),
            ],
        ),
        migrations.AddField(
            model_name='contact',
            name='groups',
            field=models.ManyToManyField(to='address_book.Group', through='address_book.GroupMember'),
        ),
        migrations.AddField(
            model_name='address',
            name='contact',
            field=models.ForeignKey(related_name='address_contact', to='address_book.Contact'),
        ),
        migrations.AlterUniqueTogether(
            name='contact',
            unique_together=set([('first_name', 'last_name')]),
        ),
    ]
