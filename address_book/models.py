from django.db import models
from django.core.urlresolvers import reverse


class Group(models.Model):
    """
        Group Model - to add new group to addressbook
    """

    name = models.CharField(max_length="100", verbose_name="Group Name", unique=True)

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("group_detail",
                       args=[self.pk])


class Contact(models.Model):
    """
        Contact Model - to add contacts to addressbook
    """

    first_name = models.CharField(max_length="100", verbose_name="First Name")
    last_name = models.CharField(max_length="100", verbose_name="Last Name")
    middle_name = models.CharField(
        max_length="100",
        verbose_name="Middle Name",
        blank=True,
        null=True)
    title = models.CharField(
        max_length="100",
        verbose_name="Title",
        blank=True,
        null=True)
    groups = models.ManyToManyField(Group, through='GroupMember')

    def __unicode__(self):
        return "%s %s" % (self.first_name, self.last_name)

    class Meta:
        unique_together = ('first_name', 'last_name',)


class GroupMember(models.Model):
    """
        Membership table between contacts and groups
    """

    contact = models.ForeignKey(Contact, related_name='membership')
    group = models.ForeignKey(Group, related_name='membership')

    def __unicode__(self):
        return "%s %s" % (self.contact, self.group)

ADDRESS_TYPE = (
    ('Home', 'Home'),
    ('Work', 'Work'),
)


class Address(models.Model):
    """
        Address Model - to add address of contact to addressbook
    """

    contact = models.ForeignKey(Contact, related_name='address_contact')
    street = models.CharField(max_length="50")
    city = models.CharField(max_length="50")
    state = models.CharField(max_length="50")
    zip = models.CharField(max_length="10")
    type = models.CharField(max_length="10", choices=ADDRESS_TYPE)

    def __unicode__(self):
        return '%s %s: %s %s, %s' % (self.contact.first_name,
                                     self.contact.last_name,
                                     self.street,
                                     self.city,
                                     self.state)

PHONE_TYPES = (
    ('Mobile', 'Mobile'),
    ('Work', 'Work'),
    ('Fax', 'Fax'),
    ('Skype', 'Skype'),
)


class PhoneNumber(models.Model):
    """
        PhoneNumber Model - to add phone details of contact to addressbook
    """

    contact = models.ForeignKey(Contact, related_name='phone_contact')
    phone = models.CharField(max_length="20")
    type = models.CharField(max_length="20", choices=PHONE_TYPES)

    def __unicode__(self):
        return "%s %s: %s" % (self.contact.first_name, self.contact.last_name, self.phone)

EMAIL_TYPES = (
    ('Personal', 'Personal'),
    ('Official1', 'Official1'),
    ('Official2', 'Official2'),
)


class Email(models.Model):
    """
        Email Model - to add email details of contact to addressbook
    """

    contact = models.ForeignKey(Contact, related_name='email_contact')
    email = models.EmailField()
    type = models.CharField(max_length="20", choices=EMAIL_TYPES)

    def __unicode__(self):
        return "%s %s: %s" % (self.contact.first_name, self.contact.last_name, self.email)
