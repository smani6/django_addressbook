Django Addressbook Project

Hosted at Openshift - http://addressbook-githubusersdata.rhcloud.com/

Application built on django with Postgres as backend

To Add contacts - http://addressbook-githubusersdata.rhcloud.com/contacts/new/
To Add Groups - http://addressbook-githubusersdata.rhcloud.com/groups/new/

To View All Groups - http://addressbook-githubusersdata.rhcloud.com/groups/
To View All Contacts - http://addressbook-githubusersdata.rhcloud.com/contacts/

To View Details About Specific Group - http://addressbook-githubusersdata.rhcloud.com/groups/1/
To View Details About Specific Contact - http://addressbook-githubusersdata.rhcloud.com/contacts/1/

Search 

Search can be done on home page itself - http://addressbook-githubusersdata.rhcloud.com/ where search fields are provided

Also separate search service is provided - http://addressbook-githubusersdata.rhcloud.com/search/
Here search can be done on emails ( exact email match or email substring match), first_name, last_name, group_name


Desgin Problem

Find person by email address (can supply any substring, ie. "comp" should work assuming "alexander@company.com" is an email address in the address book) - discuss how you would implement this without coding the solution.

In django - the above design question can be implemented using the __contains filter in the query_set - which returns all email_address matching the substring. 
The above __contains method in django - rougly transaltes to sql query like "select * from table where field like %value%";
Also can use Regex expressions to match substring in any string.





